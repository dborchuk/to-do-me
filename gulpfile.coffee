gulp = require('gulp')
runSequence = require('run-sequence')
mainBowerFiles = require('main-bower-files')
gulpFilter = require('gulp-filter')
concat = require('gulp-concat')
order = require('gulp-order')
connect = require('gulp-connect')
open = require('gulp-open')
plumber = require('gulp-plumber')
jade = require('gulp-jade')
stylus = require('gulp-stylus')
coffee = require('gulp-coffee')

paths = {
  main: './src'
  dest: './dest'
  bower: './bower_components'
}

gulp.task 'libs', ->
  filterJs = gulpFilter(['*.js'])
  gulp.src(mainBowerFiles())
    .pipe(filterJs)
    .pipe(concat('libs.js'))
    .pipe gulp.dest(paths.dest)
    .pipe(connect.reload())

  filterCss = gulpFilter(['*.css'])
  gulp.src(mainBowerFiles())
    .pipe(filterCss)
    .pipe(concat('libs.css'))
    .pipe gulp.dest(paths.dest)
    .pipe(connect.reload())
  return

gulp.task 'templates', ->
  gulp.src(paths.main + '/**/*.jade')
    .pipe(plumber())
    .pipe(jade())
    .pipe gulp.dest(paths.dest)
    .pipe(connect.reload())
  return

gulp.task 'styles', ->
  gulp.src(paths.main + '/**/*.styl')
    .pipe(concat('app.styl'))
    .pipe(stylus())
    .pipe(gulp.dest(paths.dest))
    .pipe(connect.reload())
  return

gulp.task 'scripts', ->
  gulp.src(paths.main + '/**/*.coffee')
    .pipe(plumber())
    .pipe(coffee({bare: true}))
    .pipe(order(['*/app.js']))
    .pipe(concat('app.js'))
    .pipe(gulp.dest(paths.dest))
    .pipe(connect.reload())
  return

# gulp.task 'icons', ->
#   gulp.src(paths.bower + '/material-design-icons/**/**.png')
#     .pipe(gulp.dest(paths.dest + '/files/icons'))
#   return

gulp.task 'server', ->
  connect.server({
    root: 'dest',
    livereload: true
  })
  return

gulp.task 'open', ->
  gulp.src('').pipe(open({uri: 'http://localhost:8080'}))
  return

gulp.task 'watch', ->
  gulp.watch [ paths.main + '/**/*.jade' ], [ 'templates' ]
  gulp.watch [ paths.main + '/**/*.styl' ], [ 'styles' ]
  gulp.watch [ paths.main + '/**/*.coffee' ], [ 'scripts' ]
  return

gulp.task 'default', ->
  runSequence ['templates', 'watch', 'libs', 'styles', 'scripts'], 'server', 'open'