# Authentication ctrl
injectArray = [
  'dataProvider'
  'apiPrivider'
]
dashboardCtrl = (dataProvider, apiPrivider) ->
  vm = this
  vm.todoList = null
  vm.tempEntity = null
  vm.currentTodo = null

  updateList = () ->
    apiPrivider.get().then(
      (response) ->
        vm.todoList = response
    )

  updateList()

  vm.addClickHandler = () ->
    vm.editDialog.show(vm.editDialog.MODE.CREATE)

  vm.editClickHandler = () ->
    vm.editDialog.show(vm.editDialog.MODE.EDIT)

  vm.editDialog = {
    MODE: {
      CREATE: 'CREATE'
      EDIT: 'EDIT'
    }
    isShown: false
    title: ''
    show: (mode) ->
      if mode == @MODE.CREATE
        @title = 'Create todo'
        vm.tempEntity = null
      else
        vm.tempEntity = dataProvider.getData(vm.currentTodo)
      @isShown = true
    hide: () ->
      @isShown = false
  }

  vm.submitClickHandler = (todoData) ->
    todo = null
    if todoData.id
      todo = vm.currentTodo
    dataProvider.save(todoData, todo).then(
      (response) ->
        vm.editDialog.hide()
        updateList()
      ,(response) ->
        console.log(response)
    )

  vm.doneClickHandler = () ->
    previousValue = vm.currentTodo.get('done')
    vm.currentTodo.set('done', !previousValue)
    vm.currentTodo.save().then(
      (response) ->
        updateList()
      ,(response) ->
        console.log(response)
    )

  vm.deleteClickHandler = () ->
    vm.currentTodo.destroy().then(
      (response) ->
        updateList()
      ,(response) ->
        console.log(response)
    )

  vm.setCurrentItem = (todo) ->
    vm.currentTodo = todo

  return
dashboardCtrl.$inject = injectArray

app.controller('dashboardCtrl', dashboardCtrl)