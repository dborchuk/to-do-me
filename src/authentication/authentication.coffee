# Authentication ctrl
injectArray = [
  '$scope'
  'apiPrivider'
]
authenticationCtrl = ($scope, apiPrivider) ->
  vm = this

  vm.loginClickHandler = () ->
    apiPrivider.login(vm.username, vm.password).then(
      (response) ->
        if response.id
          $scope.goTo '/'
    )

  return
authenticationCtrl.$inject = injectArray

app.controller('authenticationCtrl', authenticationCtrl)