app = angular.module('to-do-me')

injectArray = ['$http', '$q', 'dataProvider']
apiPrivider = ($http, $q, dataProvider) ->
  baseUrl = 'https://api.parse.com/1'
  Todo = dataProvider.getConstructor()
  query = new Parse.Query(Todo)

  @login = (username, password) ->
    $q(
      (resolve, reject) ->
        Parse.User.logIn(username, password).then(
          (response) ->
            resolve(response)
          (response) ->
            reject(response)
        )
    )

  @get = () ->
    $q(
      (resolve, reject) ->
        query.find({
          success: (response) ->
            resolve(response)
          error: (response) ->
            reject(response)
        })
    )

  return

apiPrivider.$inject = injectArray

app.service('apiPrivider', apiPrivider)