app = angular.module('to-do-me')

injectArray = ['$http', '$q']
dataProvider = ($http, $q) ->
  Todo = Parse.Object.extend('todoitem', {})

  @getConstructor = () ->
    return Todo

  @createObject = (data, todo) ->
    if !todo
      todo = new Todo()
    todo.set('text', data.text)
    todo.set('done', data.done)
    return todo

  @getData = (todo) ->
    data = {}
    data.text = todo.get('text')
    data.done = todo.get('done')
    data.id = todo.id
    return data

  @save = (todoData, todo) ->
    todo = @createObject(todoData, todo)
    acl = new Parse.ACL()
    acl.setWriteAccess(Parse.User.current().id, true)
    acl.setReadAccess(Parse.User.current().id, true)
    todo.setACL(acl)
    return todo.save()

  return

dataProvider.$inject = injectArray

app.service('dataProvider', dataProvider)