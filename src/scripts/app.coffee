app = angular.module('to-do-me', ['ngRoute', 'ngMaterial'])

# Config
preloader = ($q) ->
  # loaderDeferred = $q.defer()
  # promisesObject = {}

  # if !config.user
  #   promisesObject.users = getUser($q, config, usersDataProvider, tenantDataProvider)

  # promisesObject.tenantExistance = getTenantStatus($q, $http, config)

  # promisesObject.maintenanceStatus = getMaintenanceStatus($q, $http, config)

  # promisesObject.passwordRules = getPasswordComplexityLevels($q, $http, config)

  # allPromise = $q.all promisesObject

  # allPromise.then(
  #   () ->
  #     $rootScope.$broadcast('load-end')
  #     loaderDeferred.resolve()
  # )

  # return loaderDeferred.promise
  return $q (resolve) ->
    resolve()
preloader.$inject = ['$q']
# preloader.$inject = ['$q', 'config', '$http', '$rootScope', 'usersDataProvider', 'tenantsDataProvider']

app.config [
  '$routeProvider'
  ($routeProvider) ->
    $routeProvider
      .when '/',
        templateUrl: 'dashboard/dashboard.html'
        controller: 'dashboardCtrl'
        controllerAs: 'vm'
        # resolve: {
        #   preloader: preloader
        # }

      .when '/login',
        templateUrl: 'authentication/login.html'
        controller: 'authenticationCtrl'
        controllerAs: 'vm'
        # resolve: {
        #   preloader: preloader
        # }

      .otherwise redirectTo: '/'
    return
]

# Main ctrl
injectArray = [
  '$scope'
  'apiPrivider'
  '$rootScope'
  '$location'
]
mainCtrl = ($scope, apiPrivider, $rootScope, $location) ->
  vm = this

  isLoggedIn = () ->
    user = Parse.User.current()
    return user and user.id

  $rootScope.$on '$routeChangeStart', (event) ->
    if !isLoggedIn()
      $location.path '/login'
    return

  $scope.goTo = (path) ->
    $location.path path

  return
mainCtrl.$inject = injectArray

app.controller('mainCtrl', mainCtrl)